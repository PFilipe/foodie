import 'package:flutter/material.dart';
import 'package:foodie/states/ingredient_state.dart';
import 'package:provider/provider.dart';

import '../models/models.dart';
import '../states/states.dart';
import '../widgets/widgets.dart';

class IngredientsPage extends StatelessWidget {
  const IngredientsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text('Foodie'),
        ),
        body: IngredientsPageContent.fromProvider(),
        bottomNavigationBar: NavBar.fromProvider(),
        floatingActionButton: FloatingActionButton(
          onPressed: () {},
          child: const Icon(Icons.add),
        ),
      );
}

class IngredientsPageContent extends StatelessWidget {
  const IngredientsPageContent(this.state, {super.key});

  final IngredientsState state;

  List<Card> listItems(List<Ingredient> stateList) {
    List<Card> list = [];
    for (final item in stateList) {
      list.add(
        Card(
          child: ListTile(
            leading: const FlutterLogo(size: 42.0),
            title: Text(item.name),
            trailing: const Icon(Icons.more_vert),
          ),
        ),
      );
    }
    return list;
  }

  static Widget fromProvider() => Consumer<IngredientsState>(
      builder: (_, ingredientState, __) =>
          IngredientsPageContent(ingredientState));

  @override
  Widget build(BuildContext context) => ListView(
        children: listItems(state.ingredients),
      );
}
