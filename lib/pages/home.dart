import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../states/states.dart';
import '../widgets/widgets.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text('Foodie'),
        ),
        body: HomePageContent.fromProvider(),
        bottomNavigationBar: NavBar.fromProvider(),
      );
}

class HomePageContent extends StatelessWidget {
  const HomePageContent(this.navigator, {Key? key}) : super(key: key);

  final NavState navigator;

  static Widget fromProvider() => Consumer<NavState>(
      builder: (_, navState, __) => HomePageContent(navState));

  @override
  Widget build(BuildContext context) => Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MenuButton(
              label: 'Schedules',
              icon: Icons.calendar_month,
              onTap: () {
                navigator.changePage('/schedules');
              },
            ),
            const SizedBox(height: 50),
            MenuButton(
              label: 'Recipies',
              icon: Icons.menu_book,
              onTap: () {
                navigator.changePage('/recipies');
              },
            ),
            const SizedBox(height: 50),
            MenuButton(
              label: 'Shopping',
              icon: Icons.list,
              onTap: () {
                navigator.changePage('/shopping');
              },
            ),
          ],
        ),
      );
}

class MenuButton extends StatelessWidget {
  const MenuButton({
    Key? key,
    required this.label,
    required this.icon,
    this.onTap,
  }) : super(key: key);

  final String label;

  final IconData icon;

  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) => ElevatedButton(
        onPressed: onTap,
        child: Container(
          width: 300,
          height: 100,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                icon,
                size: 40,
              ),
              const SizedBox(height: 10),
              Text(label),
            ],
          ),
        ),
      );
}
