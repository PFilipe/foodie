import 'package:flutter/material.dart';

import '../widgets/widgets.dart';

class ShoppingPage extends StatelessWidget {
  const ShoppingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text('Foodie'),
        ),
        body: const ShoppingPageContent(),
        bottomNavigationBar: NavBar.fromProvider(),
      );
}

class ShoppingPageContent extends StatelessWidget {
  const ShoppingPageContent({super.key});

  @override
  Widget build(BuildContext context) => Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            Text('Shopping Page'),
          ],
        ),
      );
}
