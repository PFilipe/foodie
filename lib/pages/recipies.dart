import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../models/models.dart';
import '../states/states.dart';
import '../widgets/widgets.dart';

class RecipiesPage extends StatelessWidget {
  const RecipiesPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text('Foodie'),
        ),
        body: RecipiesPageContent.fromProvider(),
        bottomNavigationBar: NavBar.fromProvider(),
        floatingActionButton: FloatingActionButton(
          onPressed: () {},
          child: const Icon(Icons.add),
        ),
      );
}

class RecipiesPageContent extends StatelessWidget {
  const RecipiesPageContent(this.state, {super.key});

  final RecipeState state;

  List<Card> listItems(List<Recipe> stateList) {
    List<Card> list = [];
    for (final item in stateList) {
      list.add(
        Card(
          child: ListTile(
            leading: const FlutterLogo(size: 42.0),
            title: Text(item.name),
            trailing: const Icon(Icons.more_vert),
          ),
        ),
      );
    }
    return list;
  }

  static Widget fromProvider() => Consumer<RecipeState>(
      builder: (_, recipeState, __) => RecipiesPageContent(recipeState));

  @override
  Widget build(BuildContext context) => ListView(
        children: listItems(state.recipes),
      );
}
