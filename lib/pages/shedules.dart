import 'package:flutter/material.dart';
import '../widgets/widgets.dart';

class SchedulesPage extends StatelessWidget {
  const SchedulesPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text('Foodie'),
        ),
        body: const ShedulesPageContent(),
        bottomNavigationBar: NavBar.fromProvider(),
      );
}

class ShedulesPageContent extends StatelessWidget {
  const ShedulesPageContent({super.key});

  @override
  Widget build(BuildContext context) => Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            Text('Shedules Page'),
          ],
        ),
      );
}
