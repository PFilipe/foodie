import 'package:cloud_firestore/cloud_firestore.dart';

class Recipe {
  String name;
  Map<String, double> ingredients;

  Recipe({
    required this.name,
    required this.ingredients,
  });

  factory Recipe.fromFirestore(
    DocumentSnapshot<Map<String, dynamic>> snapshot,
    SnapshotOptions? options,
  ) {
    final data = snapshot.data();

    Map<String, double> ingredients = {};
    for (final ingredient in data?['ingredients'].keys) {
      final qty = data?['ingredients']?[ingredient]?['quantity'];
      ingredients[ingredient] = double.parse(qty.toString());
    }
    return Recipe(
      name: data?['name'],
      ingredients: ingredients,
    );
  }

  Map<String, dynamic> toFirestore() {
    Map<String, dynamic> dbIngredients = {};
    for (final ingredientName in ingredients.keys) {
      dbIngredients[ingredientName] = <String, dynamic>{
        'quantity': ingredients[ingredientName],
      };
    }
    return {
      'name': name,
      'ingredient': dbIngredients,
    };
  }

  String dbName() {
    return name.toLowerCase().replaceAll(' ', '_');
  }
}
