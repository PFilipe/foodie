import 'package:cloud_firestore/cloud_firestore.dart';

class Ingredient {
  Ingredient({
    required this.name,
    required this.units,
    required this.calories,
    this.brand,
    this.id,
  });

  String? id;
  String name;
  String units;
  double calories;
  String? brand;

  factory Ingredient.fromFirestore(
    DocumentSnapshot<Map<String, dynamic>> snapshot,
    SnapshotOptions? options,
  ) {
    final data = snapshot.data();

    return Ingredient(
      id: snapshot.id,
      name: data?['name'],
      units: data?['units'],
      calories: data?['calories'],
      brand: data?['brand'],
    );
  }

  Map<String, dynamic> toFirestore() => {
        'name': name,
        'units': units,
        'calories': calories,
        'brand': brand,
      };
}
