import 'dart:developer';
import 'package:cloud_firestore/cloud_firestore.dart';

import '../models/models.dart';

class IngredientService {
  IngredientService(FirebaseFirestore db)
      : _dbRef = db.collection('ingredients').withConverter(
              fromFirestore: Ingredient.fromFirestore,
              toFirestore: (Ingredient recipe, options) => recipe.toFirestore(),
            );

  final CollectionReference<Ingredient> _dbRef;

  void subscribe(void Function(QuerySnapshot<Ingredient>) onData) {
    _dbRef.snapshots().listen(onData);
  }

  void addOrUpdateIngredient(Ingredient ingredient) async {
    if (ingredient.id == '') {
      await _dbRef.add(ingredient).then(
        (_) {},
        onError: (_) {
          log('error adding ingredient to db');
        },
      );
    } else {
      await _dbRef.doc(ingredient.id).set(ingredient).then(
        (_) {},
        onError: (_) {
          log('error adding ingredient to db');
        },
      );
    }
  }
}
