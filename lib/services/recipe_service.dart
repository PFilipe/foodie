import 'dart:developer';
import 'package:cloud_firestore/cloud_firestore.dart';

import '../models/models.dart';

class RecipeService {
  RecipeService(FirebaseFirestore db)
      : _dbRef = db.collection('recipes').withConverter(
              fromFirestore: Recipe.fromFirestore,
              toFirestore: (Recipe recipe, options) => recipe.toFirestore(),
            );

  final CollectionReference<Recipe> _dbRef;

  void subcribeRecipes(void Function(QuerySnapshot<Recipe>) onData) {
    _dbRef.snapshots().listen(onData);
  }

  void addRecipe(Recipe recipe) async {
    await _dbRef.doc(recipe.name).set(recipe).then(
      (_) {},
      onError: (_) {
        log('error adding recipie to db');
      },
    );
  }
}
