import 'package:flutter/cupertino.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import '../services/services.dart';
import '../models/models.dart';

class IngredientsState extends ChangeNotifier {
  IngredientsState(this._service) {
    _service.subscribe(_load);
  }

  final IngredientService _service;

  List<Ingredient> ingredients = [];

  void _load(QuerySnapshot<Ingredient> event) {
    final List<Ingredient> newIngredients = [];
    for (final doc in event.docs) {
      newIngredients.add(doc.data());
    }
    ingredients = newIngredients;
    notifyListeners();
  }

  void addIngredient(String name, String brand, double calories, String units) {
    _service.addOrUpdateIngredient(
      Ingredient(
        name: name,
        brand: brand,
        calories: calories,
        units: units,
      ),
    );
  }

  void updateIngredient(
      String id, String name, String brand, double calories, String units) {
    _service.addOrUpdateIngredient(
      Ingredient(
        id: id,
        name: name,
        brand: brand,
        calories: calories,
        units: units,
      ),
    );
  }
}
