import 'package:flutter/cupertino.dart';

class NavState extends ChangeNotifier {
  String currentPage = '/';

  void changePage(String page) {
    currentPage = page;
    notifyListeners();
  }
}
