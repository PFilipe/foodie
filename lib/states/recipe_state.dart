import 'package:flutter/cupertino.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import '../services/services.dart';
import '../models/models.dart';

class RecipeState extends ChangeNotifier {
  RecipeState(this._service) {
    _service.subcribeRecipes(_loadRecipes);
  }

  final RecipeService _service;

  List<Recipe> recipes = [];

  void _loadRecipes(QuerySnapshot<Recipe> event) {
    final List<Recipe> newRecipes = [];
    for (final doc in event.docs) {
      newRecipes.add(doc.data());
    }
    recipes = newRecipes;
    notifyListeners();
  }
}
