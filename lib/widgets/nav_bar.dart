import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../states/states.dart';

class NavBar extends StatelessWidget {
  const NavBar({
    required this.navigator,
    super.key,
  });

  static Widget fromProvider() => Consumer<NavState>(
      builder: (_, navState, __) => NavBar(navigator: navState));

  final NavState navigator;

  void _onItemTapped(int index) {
    navigator.changePage(_pageFromIndex(index));
  }

  String _pageFromIndex(int index) {
    if (index == 0) return '/';
    if (index == 1) return '/schedules';
    if (index == 2) return '/recipies';
    if (index == 3) return '/shopping';
    if (index == 4) return '/ingredients';
    return '/';
  }

  int _indexFromPage(String page) {
    if (page == '/') return 0;
    if (page == '/schedules') return 1;
    if (page == '/recipies') return 2;
    if (page == '/shopping') return 3;
    if (page == '/ingredients') return 4;
    return 0;
  }

  @override
  Widget build(BuildContext context) => BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: _indexFromPage(navigator.currentPage),
        onTap: _onItemTapped,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.calendar_month),
            label: 'Schedules',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.menu_book),
            label: 'Recipies',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.list),
            label: 'Shopping',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_basket),
            label: 'Ingredients',
          ),
        ],
      );
}
