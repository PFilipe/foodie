import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:foodie/states/ingredient_state.dart';
import 'package:provider/provider.dart';

import 'firebase_options.dart';
import 'pages/pages.dart';
import 'services/services.dart';
import 'states/states.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  final db = FirebaseFirestore.instance;

  final recipeService = RecipeService(db);
  final ingredientService = IngredientService(db);

  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider<NavState>(
          create: (_) => NavState(),
        ),
        ChangeNotifierProvider<RecipeState>(
          create: (_) => RecipeState(recipeService),
        ),
        ChangeNotifierProvider<IngredientsState>(
          create: (_) => IngredientsState(ingredientService),
        ),
      ],
      child: FoodeeApp.fromProvider(),
    ),
  );
}

class FoodeeApp extends StatelessWidget {
  const FoodeeApp({
    required this.navigator,
    Key? key,
  }) : super(key: key);

  final NavState navigator;

  static Widget fromProvider() => Consumer<NavState>(
      builder: (_, navState, __) => FoodeeApp(navigator: navState));

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Foodie',
      theme: ThemeData(
        colorSchemeSeed: const Color(0xff007ddb),
        useMaterial3: true,
      ),
      home: Navigator(
        pages: [
          if (navigator.currentPage == '/')
            const MaterialPage(child: HomePage()),
          if (navigator.currentPage == '/schedules')
            const MaterialPage(child: SchedulesPage()),
          if (navigator.currentPage == '/recipies')
            const MaterialPage(child: RecipiesPage()),
          if (navigator.currentPage == '/shopping')
            const MaterialPage(child: ShoppingPage()),
          if (navigator.currentPage == '/ingredients')
            const MaterialPage(child: IngredientsPage()),
        ],
        onPopPage: (route, result) {
          return route.didPop(result);
        },
      ),
    );
  }
}
